package com.agileengine.rules;

import org.jsoup.nodes.Element;

import java.util.Arrays;
import java.util.List;

public class TitleRule implements LikenessRule {

    /*
    * If all the words of the original title are included in the second element, the coincidence is maximum
    * And the likeness decrease to 0 when there are no classes in common.
     */
    @Override
    public float likeness(Element original, Element other) {
        String originalTitle = original.attributes().get("title");
        String otherTitle = other.attributes().get("title");

        List<String> originalTitleWords = Arrays.asList(originalTitle.split("-"));
        List<String> otherTitleWords = Arrays.asList(otherTitle.split("-"));

        int commonWords = (int) otherTitleWords.stream().filter(originalTitleWords::contains).count();

        return (float) commonWords / otherTitleWords.size();
    }
}
