package com.agileengine.rules;

import org.jsoup.nodes.Element;

import java.util.Arrays;
import java.util.List;

public class ClassRule implements LikenessRule {

    /*
    * If all the words of the original class are included in the second element, the coincidence is maximum
    * And the likeness decrease to 0 when there are no classes in common.
     */
    @Override
    public float likeness(Element original, Element other) {
        String originalClass = original.attributes().get("class");
        String otherClass = other.attributes().get("class");

        List<String> originalClasses = Arrays.asList(originalClass.split(" "));
        List<String> otherClasses = Arrays.asList(otherClass.split(" "));

        int commonClasses = (int) otherClasses.stream().filter(originalClasses::contains).count();

        return (float) commonClasses / otherClasses.size();
    }
}
