package com.agileengine.rules;

import org.jsoup.nodes.Element;

public interface LikenessRule {
    /*
    * a float from 0 to 1 indicating how similar some elements are
    * 0 -> Totally different
    * 1 -> Totally equal
    * */
    public float likeness(Element original, Element other);
}
