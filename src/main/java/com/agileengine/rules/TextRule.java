package com.agileengine.rules;

import org.jsoup.nodes.Element;

import java.util.Arrays;
import java.util.List;

public class TextRule implements LikenessRule {

    /*
    * If all the words of the original title are included in the second element, the coincidence is maximum
    * And the likeness decrease to 0 when there are no classes in common.
     */
    @Override
    public float likeness(Element original, Element other) {
        String originalText = original.text();
        String otherText = other.text();

        List<String> originalTextWords = Arrays.asList(originalText.split(" "));
        List<String> otherTextWords = Arrays.asList(otherText.split(" "));

        int commonWords = (int) otherTextWords.stream().filter(originalTextWords::contains).count();

        return (float) commonWords / otherTextWords.size();
    }
}
