package com.agileengine;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Spliterator;

public class ElementFinder {
    private static Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public Element findElementById(File htmlFile, String targetElementId) {
        Document doc = toDocument(htmlFile);

        Element element = doc.getElementById(targetElementId);
        if (element == null) {
            LOGGER.error("Id [{}] not found in file [{}]", targetElementId, htmlFile.getAbsolutePath());
            throw new IllegalArgumentException("Id not found");
        }

        return element;
    }

    public Spliterator<Element> findAllElements(File htmlFile) {
        return toDocument(htmlFile).getAllElements().spliterator();
    }

    private Document toDocument(File htmlFile) {
        try {
            return Jsoup.parse(htmlFile, "utf8");
        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", htmlFile.getAbsolutePath(), e);
            throw new IllegalArgumentException("Cannot read provided file");
        }
    }
}
