package com.agileengine;

import com.agileengine.rules.ClassRule;
import com.agileengine.rules.LikenessRule;
import com.agileengine.rules.TextRule;
import com.agileengine.rules.TitleRule;
import javafx.util.Pair;
import org.jsoup.nodes.Element;

import java.util.Arrays;
import java.util.List;

public class LikenessCalculator {
    public List<Pair<LikenessRule, Integer>> rulesAndWeight =
            Arrays.asList(
                new Pair<>(new TextRule(), 50),
                new Pair<>(new ClassRule(), 50),
                new Pair<>(new TitleRule(), 30)
            );

    /*
    * returns a float indicating how likely are 2 given elements
    * 1 indicates the elements are very likely
    * 0 indicates the elements are very different
     */
    public float likeness(Element target, Element other, boolean logging) {
        if (target.id().equals(other.id())) {
            if (logging) {
                System.out.println("Matching id");
                System.out.println("Overall likeness: 1");
            }
            return 1; // If the ids are equal we are done
        }

        if (other.attributes().get("style").replaceAll(" ", "").contains("display:none")) {
            if (logging) {
                System.out.println("Hidden element");
                System.out.println("Overall likeness: 0");
            }
            return 0; // It's a trap!
        }

        int maximumLikeness = rulesAndWeight.stream().map(Pair::getValue).reduce(0, Integer::sum);

        float actualLikeness = rulesAndWeight.stream().map((rulesAndWeight) -> {
            float likeness = rulesAndWeight.getKey().likeness(target, other);
            if (logging) {
                System.out.println(rulesAndWeight.getKey().getClass().getSimpleName() + " likeness: " + likeness);
            }
            return likeness * rulesAndWeight.getValue();
        }).reduce(0f, Float::sum);

        float overallLikeness = (actualLikeness / maximumLikeness) * 0.9f;
        if (logging) {
            System.out.println("Overall likeness: " + overallLikeness);
        }
        return overallLikeness; // Maximum without equal id is 0.9
    }

    public float likeness(Element target, Element other) {
        return likeness(target, other, false);
    }
}
