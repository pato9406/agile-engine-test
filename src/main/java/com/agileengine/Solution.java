package com.agileengine;

import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.stream.StreamSupport;

public class Solution {
    public static final String DEFAULT_ID = "make-everything-ok-button";
    private static Logger LOGGER = LoggerFactory.getLogger(Solution.class);

    public static void main(String[] args) {
        if (args.length < 2) {
            throw new IllegalArgumentException("You need to provide 2 xml files as arguments");
        }

        String originalFilePath = args[0];
        String destFilePath = args[1];

        String targetElementId;
        if (args.length > 2) {
            targetElementId = args[2];
        } else {
            LOGGER.info("targetElementId not passed as argument, assuming default: [{}]", DEFAULT_ID);
            targetElementId = DEFAULT_ID;
        }

        ElementFinder elementFinder = new ElementFinder();
        Element targetElement = elementFinder.findElementById(new File(originalFilePath), targetElementId);
        Spliterator<Element> elements = elementFinder.findAllElements(new File(destFilePath));

        LikenessCalculator calculator = new LikenessCalculator();
        Element matching = StreamSupport.stream(elements, false)
                .max((e1, e2) -> Float.compare(calculator.likeness(targetElement, e1), calculator.likeness(targetElement, e2)))
                .get();

        System.out.println("");
        System.out.println("Best matching element path: " + new PathPrinter().getElementPath(matching));
        System.out.println("");

        System.out.println(" --- Explanation ---");
        System.out.println("Source element: " + targetElement);
        System.out.println("Matching element: " + matching);
        System.out.println("");
        calculator.likeness(targetElement, matching, true);
    }
}
