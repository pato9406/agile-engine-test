package com.agileengine;

import org.jsoup.nodes.Element;

public class PathPrinter {
    public String getElementPath(Element element) {
        /*
        * At first, I tried to construct the XPath by myself, but after some debugging I decided to
        * go with css selector. It's not exactly the requested format, but it's probably far more
        * accurate than what I can do in a couple of hours
         */
        return element.cssSelector();
    }
}
