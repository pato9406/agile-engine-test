# Agile Engine - Solution

This solution is based on the code from the repo found at https://bitbucket.org/agileengine/ae-backend-xml-java-snippets

The code is uploaded with a corresponding jar. The jar can be built by doing `./gradlew build`

### Example run
```
java -jar solution.jar input/sample-0-origin.html input/sample-4-the-mash.html
```

New rules can be easily added by adding a new implementation of a `LikenessRule`, and adding it to the list of rules in the `LikenessCalculator`, with a corresponding weight

